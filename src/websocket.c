#include "websocket.h"
#include "messages.h"
#include "http.h"
#include "frame_parser.h"
#include "defragmenter.h"

#include <unistd.h>
#include <sys/socket.h>
#include <string.h>

struct sWebsocket {
    // http header related
    int valid_header;
    bytestring header;
    char* host;

    // parser
    sFrameParser *frame_parser;
    sDefragmenter *defragmenter;

    // io
    int socket;
    sSendBuffer* send_buffer;

    enum websocket_status status;
    int id;
    int flags;
};

static ws_on_send_bytes_callback_t ws_on_send_bytes_callback = ws_default_on_send_bytes_callback;
void set_on_send_bytes_callback(ws_on_send_bytes_callback_t callback) {
    ws_on_send_bytes_callback = callback;
}

sWebsocket* sWebsocket_new() {
    return (sWebsocket*) malloc(sizeof(sWebsocket));
}

int sWebsocket_create(sWebsocket* ws, int socket, int wsid, const char* host, int flags) {
    ws->socket = socket;
    ws->flags = flags;

    // set wsid
    if (wsid == 0)
        wsid = socket;
    ws->id = wsid;

    // set status
    ws->valid_header = 0;
    ws->status = WEBSOCKET_OPEN;
    
    // parser
    if ((ws->frame_parser = sFrameParser_new()) == NULL)
        return 0;

    // http related
    if (bytestring_create_empty(&ws->header,0) == 0)
        return 0;
    wsLog_system("host: len:%lu, %s\n", strlen(host), host);
    if((ws->host = (char*)malloc(strlen(host))) == NULL) {
        return 0;
    }
    memcpy(ws->host, host, strlen(host));

    if ((flags & DISABLE_SEND_BUFFER) == 0) {
        ws->send_buffer = (sSendBuffer*)malloc(sizeof(sSendBuffer));
        if (!sSendBuffer_create(ws->send_buffer))
            return 0;
    }
    return 1;
}

int sWebsocket_get_id(sWebsocket* ws) {
    return ws->id;
}

int sWebsocket_recv(sWebsocket* ws, sMessage* messages, size_t* msg_count) {
    char buf[256];
    int nbytes;
    if ((nbytes = recv(ws->socket, buf, sizeof buf, 0)) <= 0) {
        *msg_count = 0;
        if (nbytes == 0) {
            sWebsocket_shutdown(ws, CLOSE_NORMAL);
            return -CLOSE_NORMAL;
        } else {
            sWebsocket_shutdown(ws, CLOSE_SOCK_RECV_ERROR);
            return -CLOSE_SOCK_RECV_ERROR;
        }
    } else {
        bytestring str;
        bytestring_create(&str, buf, nbytes);
        wsLog_in("[%d] < ", ws->id);
        bytestring_print(&str);
        bytestring_delete(&str);
        int status;
        if ((status = sWebsocket_add_bytes(ws, buf, nbytes, messages, msg_count)) <= 0) {
            sWebsocket_shutdown(ws, -status);
            return status;
        }
    }
    return nbytes;
}

void sWebsocket_shutdown(sWebsocket* ws, enum close_codes close_code) {
    ws->status = WEBSOCKET_CLOSED;
    if (close_code == CLOSE_HTTP_BADREQUEST) {
        bytestring badreq = http_response400();
        ws_on_send_bytes_callback(ws, badreq.data, badreq.len);
        bytestring_delete(&badreq);
    } else if (close_code == CLOSE_HTTP_INTSERVERR) {
        bytestring intserverr = http_response500();
        ws_on_send_bytes_callback(ws, intserverr.data, intserverr.len);
        bytestring_delete(&intserverr);
    } else if (close_code == CLOSE_NORMAL) {
        // if we got an error or the connection is already closed
        // we don't want to send anything anymore
        sSendbuffer_erase(ws->send_buffer);
        wsLog_system("[%d] socket hung up\n", ws->id);
    } else if (close_code == CLOSE_SOCK_RECV_ERROR) {
        sSendbuffer_erase(ws->send_buffer);
    } else if (close_code == CLOSE_OK) {
        wsLog_system("[%d] normal shutdown\n", ws->id);
    }else {
        sFrame close_frame;
        wsLog_out("[%d] send close\n", ws->id);
        sFrame_create_close(&close_frame, close_code, NULL, 0, 0);
        wsLog_out("[%d] > ", ws->id);
        sFrame_print(&close_frame); nl();
        wsLog_out("[%d] encoded > ", ws->id);
        bytestring_print(&close_frame.bytes);
        ws_on_send_bytes_callback(ws, close_frame.bytes.data, close_frame.bytes.len);
        sFrame_delete(&close_frame);
    }
}


int sWebsocket_add_bytes(sWebsocket* ws, char* buf, size_t nbytes, sMessage* messages, size_t* msg_count) {
    size_t max_msg_count = *msg_count;
    *msg_count = 0;
    // we got some data from a client
    if (ws->valid_header) {
        sMessage* msg = &messages[*msg_count];
        sFrame* control_frame;
        sFrameParser* parser = ws->frame_parser;
        sFrameParser_addBytes(parser, buf, nbytes);
        while(1) {
            sFrame reply_frame;
            enum frame_parser_result fp_result;
            switch (fp_result = sFrameParser_parse(parser, msg, &control_frame)) {
                case FPARSER_RESULT_PING:
                    sFrame_create_pong(&reply_frame, control_frame);
                    wsLog_in("[%d] received ping! -> send pong\n", ws->id);
                    wsLog_out("[%d] > ", ws->id);
                    sFrame_print(&reply_frame); nl();
                    wsLog_out("[%d] encoded > ", ws->id);
                    bytestring_print(&reply_frame.bytes); nl();
                    ws_on_send_bytes_callback(ws, reply_frame.bytes.data, reply_frame.bytes.len);
                    sFrame_delete(&reply_frame);
                    break;
                case FPARSER_RESULT_CLOSE:
                    sFrame_create_close(&reply_frame, 1000, NULL, 0, 0);
                    wsLog_in("[%d] received close! -> send close\n", ws->id);
                    wsLog_out("[%d] > ", ws->id);
                    sFrame_print(&reply_frame); nl();
                    wsLog_out("[%d] encoded > ", ws->id);
                    bytestring_print(&reply_frame.bytes); nl();
                    ws->status = WEBSOCKET_CLOSED;
                    ws_on_send_bytes_callback(ws, reply_frame.bytes.data, reply_frame.bytes.len);
                    sFrame_delete(&reply_frame);
                    return -CLOSE_OK; // TODO: reason 1000 is correct??
                case FPARSER_RESULT_MESSAGE:
                    wsLog_in("[%d] got new message!\n", ws->id);
                    (*msg_count)++;
                    break;
                case FPARSER_RESULT_COMPLETE:
                    return 1;
                default:
                    return fp_result;
            }
            sFrameParser_reset(parser);
            if ((*msg_count) >= max_msg_count) {
                break;
            }
        }
    // end if (ws->valid_header)
    } else {
        bytestring response, key;
        enum header_status status;
        bytestring_add_chararray(&ws->header, buf, nbytes);

        status = validate_header(&ws->header, &key, ws->host);
        if(status == HEADER_VALID)
        {
            wsLog_in("[%d] < ", ws->id);
            bytestring_print(&ws->header); nl();
            http_response_websocket_upgrade(&response, &key);
            wsLog_out("[%d] > ", ws->id);
            bytestring_print(&response); nl();
            ws_on_send_bytes_callback(ws, response.data, response.len);
            ws->valid_header = 1;
            bytestring_delete(&ws->header);
            if (!(sFrameParser_create(ws->frame_parser, 1))) {
                wsLog_error("memory allocation error, while creating sFrameParser!\n");
                return -CLOSE_HTTP_INTSERVERR;
            }
        } else if (status == HEADER_INVALID) {
            bytestring_delete(&ws->header);
            wsLog_error("[%d] invalid header!\n", ws->id);
            return -CLOSE_HTTP_BADREQUEST;
        } else if (status == HEADER_MEM_ERROR) {
            bytestring_delete(&ws->header);
            wsLog_error("[%d] memory error!\n", ws->id);
            return -CLOSE_HTTP_INTSERVERR;
        }
    }
    return 1;
}

int sWebsocket_sendBuffer_send(sWebsocket* ws, size_t max_send_len) {
    return sSendBuffer_send(ws->send_buffer, ws->socket, max_send_len);
}

int sWebsocket_send_message(sWebsocket* ws, sMessage* msg) {
    sFrame frame;
    int status;
    sMessage_to_frame(msg, &frame);
    status = sWebsocket_send_frame(ws, &frame);
    sFrame_delete(&frame);
    return status;
}


int sWebsocket_send_frame(sWebsocket* ws, sFrame* frame) {
    wsLog_out("[%d] > ", ws->id);
    sFrame_print(frame); nl();
    wsLog_out("[%d] encoded > ", ws->id);
    bytestring_print(&frame->bytes); nl();
    return ws_on_send_bytes_callback(ws, frame->bytes.data, frame->bytes.len);
}

void sWebsocket_delete(sWebsocket* ws) {
    wsLog_system("[%d] close socket!\n", ws->id);
    if (ws->socket != 0)
        close(ws->socket);
    if (!(ws->valid_header))
        bytestring_delete(&ws->header);
    else {
        sFrameParser_delete(ws->frame_parser);
        free(ws->frame_parser);
    }
    if ((ws->flags & DISABLE_SEND_BUFFER) == 0) {
        sSendBuffer_delete(ws->send_buffer);
        free(ws->send_buffer);
    }
}

int sWebsocket_sendBuffer_empty(sWebsocket* ws) {
    return ws->send_buffer->len == 0;
}

int sWebsocket_is_open(sWebsocket* ws) {
    return ws->status == WEBSOCKET_OPEN;
}

int ws_default_on_send_bytes_callback(sWebsocket* ws, char* bytes, size_t len) {
    if (ws->send_buffer != NULL)
        return sSendBuffer_add_bytes(ws->send_buffer, bytes, len);
    return 0;
}
