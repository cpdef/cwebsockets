#include "frame_parser.h"

#include "log.h"
#include "utf-8.h"
#include "defragmenter.h"
#include "websocket.h"

enum frame_parser_status {
    FPARSER_COMPLETE,
    FPARSER_FIN_RSV_OPCODE,
    FPARSER_PAYLOADLEN,
    FPARSER_MASKKEY,
    FPARSER_DATA,
    FPARSER_FINALIZE,
    FPARSER_E_WRONG_MASK,
    FPARSER_E_CONTROLFRAME_TO_LONG,
    FPARSER_E_NO_UTF8_IN_TEXT
};

typedef struct sFrameParser {
    int from_client;
    size_t parsed_bytes;
    enum frame_parser_status status;
    sFrame frame;
    size_t need_bytes;
    size_t available_bytes;
    sDefragmenter* defragmenter;
} sFrameParser;

sFrameParser* sFrameParser_new() {
    return malloc(sizeof(sFrameParser));
}

int sFrameParser_create(sFrameParser* parser, int is_server) {
    parser->from_client = is_server ? 1 : 0;
    if ((parser->defragmenter = sDefragmenter_new()) == NULL)
        return 0;
    sDefragmenter_create(parser->defragmenter);
    parser->parsed_bytes = 0;
    bytestring_create_empty(&parser->frame.bytes, 0);
    if (!sFrameParser_reset(parser)) {
        return 0;
    }
    return 1;
}

int sFrameParser_reset(sFrameParser* parser) {
    bytestring_shrink(&parser->frame.bytes, parser->parsed_bytes, 0);

    parser->parsed_bytes = 0;
    // we need at least 2 bytes to start parsing
    parser->need_bytes = 2;
    parser->available_bytes = 0;
    parser->status = FPARSER_FIN_RSV_OPCODE;
    return 1;
}

uint64_t u64_from_bytes(char* bytes, int nbytes) {
    uint64_t result = 0;
    for (int i=0; i < nbytes; i++) {
        result = result*0x100;
        result += (unsigned char) bytes[i];
    }
    return result;
}

void sFrameParser_addBytes(sFrameParser* parser, char* buffer, int nbytes) {
    bytestring_add_chararray(&parser->frame.bytes, buffer, nbytes);
    parser->available_bytes = parser->frame.bytes.len - parser->parsed_bytes;
}

int sFrameParser_parse_frame(sFrameParser* parser) {
    sFrame* frame = &parser->frame;
    bytestring* bytes = &frame->bytes;
    switch (parser->status) {
        case FPARSER_FIN_RSV_OPCODE:
            parser->need_bytes = 2;
            if (bytes->len >= parser->parsed_bytes+parser->need_bytes) {
                frame->FIN = (bytes->data[0] & 0x80) >> 7; // first bit
                frame->RSV = (bytes->data[0] & 0x70) >> 4; // second to fourth bit
                frame->OPCODE = bytes->data[0] & 0xf; // last 4 bits
                frame->MASK = (bytes->data[1] & 0x80) >> 7; // first bit

                // client MUST mask the frame
                // server MUST NOT mask the frame
                if (frame->MASK != parser->from_client)
                {
                    parser->need_bytes = 0;
                    parser->status = FPARSER_E_WRONG_MASK;
                }

                frame->PAYLOADLEN = bytes->data[1] & 0x7f; // last 7 bits
                parser->parsed_bytes = 2;
                parser->status = FPARSER_PAYLOADLEN;

                // All control frames MUST have a payload length of 125 bytes or less
                // and MUST NOT be fragmented.
                if (frame->OPCODE >= OPCODE_CLOSE) // control frame
                {
                    if ((frame->PAYLOADLEN > 125) || (frame->FIN != 1)) {
                        parser->need_bytes = 0;
                        parser->status = FPARSER_E_CONTROLFRAME_TO_LONG;
                    }
                }
            }
        case FPARSER_PAYLOADLEN:
            if (frame->PAYLOADLEN == 126) {
                parser->need_bytes = 2;
                if (bytes->len >= parser->parsed_bytes+parser->need_bytes) {
                    frame->PAYLOADLEN = u64_from_bytes(&bytes->data[2], 2);
                    parser->parsed_bytes += 2;
                } else break;
            } else if (frame->PAYLOADLEN == 127) {
                parser->need_bytes = 8;
                if (bytes->len >= parser->parsed_bytes+parser->need_bytes) {
                    frame->PAYLOADLEN = u64_from_bytes(&bytes->data[2], 8);
                    parser->parsed_bytes += 8;
                } else break;
            }
            parser->status = FPARSER_MASKKEY;
        case FPARSER_MASKKEY:
            if (parser->from_client) {
                parser->need_bytes = 4;
                if (bytes->len >= parser->parsed_bytes+parser->need_bytes) {
                    frame->MASKKEY = parser->parsed_bytes;
                    parser->parsed_bytes += 4;
                    parser->status = FPARSER_DATA;
                } else break;
            } else {
                parser->status = FPARSER_DATA;
            }
        case FPARSER_DATA:
            parser->need_bytes = frame->PAYLOADLEN;
            if (bytes->len >= parser->parsed_bytes+parser->need_bytes) {
                frame->DATA = parser->parsed_bytes;
                if (parser->from_client) {
                    // we unmask the bytes inplace
                    for (int i=0; i < frame->PAYLOADLEN;i++)
                    {
                        int j = i % 4;
                        bytes->data[frame->DATA+i] ^= bytes->data[frame->MASKKEY+j];
                    }
                }
                parser->parsed_bytes += frame->PAYLOADLEN;
                parser->status = FPARSER_FINALIZE;
            } else break;
        case FPARSER_FINALIZE:
            parser->need_bytes = 0;
            if ((frame->OPCODE == OPCODE_TEXT) && !check_utf8(&bytes->data[frame->DATA], frame->PAYLOADLEN)) {
                parser->status = FPARSER_E_NO_UTF8_IN_TEXT;
                break;
            }
            parser->status = FPARSER_COMPLETE;
        default:
            break;
    }
    parser->available_bytes = bytes->len-parser->parsed_bytes;
    return parser->status;
}

int sFrameParser_parse(sFrameParser* parser, sMessage* message, sFrame** control_frame) {
    sFrame* frame;
    while ((parser->available_bytes >= parser->need_bytes)) {
        int parse_status = sFrameParser_parse_frame(parser);
        switch (parser->status) {
            case FPARSER_COMPLETE:
                frame = &parser->frame;
                wsLog_in("[parser] parsed <");
                sFrame_print(frame); nl();
                if (frame->OPCODE == OPCODE_PING) {
                    *control_frame = &parser->frame;
                    return FPARSER_RESULT_PING;
                } else if (frame->OPCODE == OPCODE_CLOSE) {
                    *control_frame = &parser->frame;
                    return FPARSER_RESULT_CLOSE;
                } else {
                    int defr_status;
                    if ((defr_status = sDefragmenter_run(parser->defragmenter, frame, message)) == 1) {
                        return FPARSER_RESULT_MESSAGE;
                    }
                    else if (defr_status < 0) {
                        return defr_status;
                    }
                }
                break;
            case FPARSER_E_NO_UTF8_IN_TEXT:
                return -CLOSE_NO_UTF8_TEXT_INCONSISTENCY;
            case FPARSER_E_WRONG_MASK:
            case FPARSER_E_CONTROLFRAME_TO_LONG:
                return -CLOSE_PROTOCOL_ERROR;
            default: // parser didn't finish, because it got not enought bytes
                break;
        } // end switch
    } // end while
    return FPARSER_RESULT_COMPLETE;
}


void sFrameParser_delete(sFrameParser* parser) {
    sFrame_delete(&parser->frame);
    free(parser->defragmenter);
}
