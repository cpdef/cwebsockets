#include "utf-8.h"
int check_utf8(char* string, size_t len) {
    int symbol_len;
    unsigned char first_byte;
    int symbol_index = 0;
    for (size_t i=0; i < len; i++) {
        unsigned char val = string[i];
        if (symbol_index == 0) {
            // valid ascii
            if (val <= 0x7f) {
                continue;
            // range from 0x80 to 0xC1 not allowed in first byte
            } else if (val <= 0xC1 ) {
                return 0;
            // start of a sequence longer then 4 bytes or 4 bytes over 140000
            // is not allowed
            } else if (val >= 0xF5) {
                return 0;
            } else {
                first_byte = val;
                if (val >= 0xF0)
                    symbol_len = 4;
                else if (val >= 0xE0)
                    symbol_len = 3;
                else
                    symbol_len = 2;
                symbol_index++;
            }
        } else {
            if ((val < 0x80) || (val > 0xBF)) // not matching 10xxxxxx
                return 0;

            if (symbol_index == 1) {
                // this could be encoded with 2 bytes!
                if ((first_byte == 0xE0) && (val < 0xA0))
                    return 0;
                // dissallow surrogates (see CESU)
                if ((first_byte == 0xED) && (val >= 0xA0))
                    return 0;
                // this could be encoded with 3 bytes!
                if ((first_byte == 0xF0) && (val < 0x90))
                    return 0;
                // this isn't allowed, idk why though
                if ((first_byte == 0xF4) && (val > 0x8F))
                    return 0;
            }
            symbol_index++;
            if (symbol_index >= symbol_len)
                symbol_index = 0;
        }

    }
    if (symbol_index == 0)
        return 1;
    else
        return 0;
}
