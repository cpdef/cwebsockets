#include "messages.h"
#include <string.h>

void sMessage_delete(sMessage* message) {
    bytestring_delete(&message->data);
}

int sMessage_create(sMessage* message, char* data, size_t len, int is_utf8) {
    if (bytestring_create(&message->data, data, len))
    {
        message->is_utf8 = is_utf8;
        return 1;
    }
    return 0;
}

void sMessage_to_frame(sMessage* message, sFrame* frame) {
    char* fdata;
    if (message->is_utf8)
        sFrame_create(frame, message->data.len, 1, 0, OPCODE_TEXT, 0);
    else
        sFrame_create(frame, message->data.len, 1, 0, OPCODE_BINARY, 0);
    fdata = sFrame_get_data_ptr(frame);
    memcpy(fdata, message->data.data, message->data.len);

}
