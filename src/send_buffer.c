#include "send_buffer.h"

#include <string.h>
#include <sys/socket.h>
#include <stdint.h>

int sSendBuffer_create(sSendBuffer* buffer) {
    buffer->internal_size = 100;
    buffer->len = 0;
    buffer->start = 0;
    if ((buffer->buf = malloc(100)) == NULL)
        return 0;
    return 1;
}

void sSendBuffer_delete(sSendBuffer* buffer) {
    if (buffer->buf != NULL)
        free(buffer->buf);
    buffer->buf = NULL;
}

void sSendbuffer_erase(sSendBuffer* buffer) {
    sSendBuffer_delete(buffer);
    buffer->internal_size = 0;
    buffer->len = 0;
    buffer->start = 0;
    buffer->buf = NULL;
}

int sSendBuffer_add_bytes(sSendBuffer* buffer, char* bytes, int len) {
    size_t add_index = (buffer->start+buffer->len) % buffer->internal_size;

    // resize the buffer if its not big enought anymore
    if (len > (buffer->internal_size-buffer->len))
    {
        if ((buffer->buf = realloc(buffer->buf, buffer->internal_size*2)) == 0)
            return 0;
        // if the size is dubbled, the data has not to be divided anymore
        // copy all bytes which are placed before start to the end
        if (add_index < buffer->start) {
            memcpy(&buffer->buf[buffer->internal_size], buffer, add_index-1);
            add_index = buffer->start+buffer->len;
        }
        buffer->internal_size = buffer->internal_size*2;
    }
    while (len > (buffer->internal_size-buffer->len))
    {
        if ((buffer->buf = realloc(buffer->buf, buffer->internal_size*2)) == 0)
            return 0;
        buffer->internal_size = buffer->internal_size*2;
    }

    // add the new bytes
    if ((add_index+len) < buffer->internal_size) {
        memcpy(&buffer->buf[add_index], bytes, len);
    } else {
        int space_at_end = buffer->internal_size-add_index;
        memcpy(&buffer->buf[add_index], bytes, space_at_end);
        memcpy(buffer->buf, &bytes[space_at_end], len-space_at_end);
    }
    buffer->len += len;
    return 1;
}

int sSendBuffer_send(sSendBuffer* buffer, int socket, size_t max_send_len) {
    size_t nbytes;
    char *send_begin = &buffer->buf[buffer->start];
    size_t send_len;
    if ((buffer->len == 0) || (buffer->internal_size == 0))
        return 0;
    if (max_send_len == 0)
        max_send_len = SIZE_MAX;
    size_t end_index = (buffer->start+buffer->len) % buffer->internal_size;
    if (end_index > buffer->start) {
        send_len = buffer->len;
    } else {
        send_len = buffer->internal_size-buffer->start;
    }
    if (send_len > max_send_len)
        send_len = max_send_len;
    nbytes = send(socket, send_begin, send_len, 0);
    if (nbytes < 0)
        return nbytes;

    // correct len and start
    buffer->len -= nbytes;
    if (buffer->len == 0) {
        buffer->start = 0;
    } else {
        buffer->start = (buffer->start+nbytes) % buffer->internal_size;
    }
    return nbytes;
}
