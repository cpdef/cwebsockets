#include "defragmenter.h"
#include "websocket.h"

typedef struct sDefragmenter {
    int first_fragment;
    enum opcode first_fragment_opcode;
    bytestring frame_data;
} sDefragmenter;

int sDefragmenter_create(sDefragmenter* df) {
    df->first_fragment = 1;
    return 1;
}

sDefragmenter* sDefragmenter_new() {
    return (sDefragmenter*)malloc(sizeof(sDefragmenter));
}

int sDefragmenter_run(sDefragmenter* df, sFrame* frame, sMessage* msg) {
    enum opcode current_oc = frame->OPCODE;
    if (frame->FIN == 1) {
        if (df->first_fragment == 1) {
            if (current_oc == OPCODE_CONTINUATION)
                return -CLOSE_PROTOCOL_ERROR;
            df->first_fragment_opcode = current_oc;
            if (bytestring_create_empty(&df->frame_data,0) == 0)
                return -CLOSE_UNEXPECTED_CONDITION;
        }

        if (!bytestring_add_chararray(
                &df->frame_data, sFrame_get_data_ptr(frame),
                frame->PAYLOADLEN
        )) return -CLOSE_UNEXPECTED_CONDITION;

        int is_utf8;
        if (df->first_fragment_opcode == OPCODE_BINARY) {
            is_utf8 = 0;
        } else if (df->first_fragment_opcode == OPCODE_TEXT) {
            is_utf8 = 1;
        } else {
            return -CLOSE_UNEXPECTED_CONDITION;
        }
        sMessage_create(msg, df->frame_data.data, df->frame_data.len, is_utf8);
        df->first_fragment = 1;
        return 1;
    } else {
        if (df->first_fragment == 1) {
            if (frame->OPCODE == OPCODE_CONTINUATION)
                return -CLOSE_PROTOCOL_ERROR;
            df->first_fragment_opcode = frame->OPCODE;

            if (bytestring_create_empty(&df->frame_data,0) == 0)
                return -CLOSE_UNEXPECTED_CONDITION;
        } else {
            if (frame->OPCODE != OPCODE_CONTINUATION)
                return -CLOSE_PROTOCOL_ERROR;
        }

        if (!bytestring_add_chararray(
                &df->frame_data, sFrame_get_data_ptr(frame),
                frame->PAYLOADLEN
        )) return -CLOSE_UNEXPECTED_CONDITION;
        df->first_fragment = 0;
    }
    return 0;
}
