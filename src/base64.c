#include "base64.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char b64_table[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'
};

typedef unsigned char uchar;

char* char_3_b64_4(char* three, int len) {
    char* four;
    if (!(four = malloc(sizeof(char)*4)))
        return NULL;
    four[0] = '=';
    four[1] = '=';
    four[2] = '=';
    four[3] = '=';
    const int mask = 64-1;
    if (len > 0)
        // first 6 bits of the first byte
        four[0] = b64_table[((uchar)three[0] & (mask << 2)) >> 2];
    if (len > 1) {
        // last 2 bits of the first byte
        // + first 4 bits of the second byte
        four[1] = b64_table[ (((uchar)three[0] & (mask >> 4) ) << 4)
                            +(((uchar)three[1] & (mask << 2) ) >> 4)];
        if (len > 2) {
            // last 4 bits of second byte
            // + first 2 bits of third byte
            four[2] = b64_table[ (((uchar)three[1] & (mask >> 2) ) << 2)
                                +(((uchar)three[2] & (mask << 6) ) >> 6)];
            four[3] = b64_table[(uchar)three[2] & mask];
        }
        else
            // last 4 bits of second byte + 2x Zero
            four[2] = b64_table[((uchar)three[1] & (mask >> 2) ) << 2];
    }
    else
        // last 2 bits of the first byte + 4x Zero
        four[1] = b64_table[((uchar)three[0] & (mask >> 4) ) << 4];
    return four;
}

int b64encode(bytestring* input, bytestring* string) {
    int len = input->len / 3;
    int remainder = input->len % 3;
    int total_len = len*4;
    char* four;
    if (remainder)
        total_len += 4;
    if (!(string->data = malloc(sizeof(char)*(total_len))))
        return 0;
    string->len = total_len;
    for (int i = 0; i < len; i++)
    {
        four = char_3_b64_4(&input->data[i*3], 3);
        memcpy(&string->data[i*4], four, 4);
    }
    if (remainder) {
        four = char_3_b64_4(&input->data[len*3], remainder);
        memcpy(&string->data[len*4], four, 4);
    }
    return 1;
}
