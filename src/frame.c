#include "frame.h"
#include <stdio.h>
#include <string.h>

void sFrame_print(sFrame* frame) {
    printf("FIN: %d, RSV: %d, OPCODE: 0x%01x, PLEN: %ld, MASK: %d",
        frame->FIN, frame->RSV, frame->OPCODE, frame->PAYLOADLEN, frame->MASK
    );
    if (frame->OPCODE == OPCODE_CLOSE) {
        printf(", close-reason: %u (",
            ((((unsigned char)frame->bytes.data[frame->DATA]) << 8)
             + (unsigned char)frame->bytes.data[frame->DATA+1])
        );
        bytestring_print_substr(&frame->bytes, frame->DATA+2,0);
        printf(")");
    } else {
        printf(", DATA: ");
        bytestring_print_substr(&frame->bytes, frame->DATA,0);
    }
}

int sFrame_create(
        sFrame* frame, size_t len, int FIN, int RSV, int OPCODE, int MASK
    ) {
    int payloadlen_bytes = 0; // doesn't include the bits if < 126
    bytestring* bytes = &frame->bytes;
    bytes->len = 2; // FIN, RSV, MASK, OPCODE, first PAYLOADLEN bits
    frame->FIN = FIN;
    frame->RSV = RSV;
    frame->OPCODE = OPCODE;
    frame->MASK = MASK;
    frame->MASKKEY = 0;

    // PAYLOADLEN bytes
    if (len > 126)
        bytes->len += 2;
    if (len > 0xffff)
        bytes->len += 6; // in total 8 more

    // MASKKEY bytes
    if (MASK)
        bytes->len += 4;

    // DATA bytes
    bytes->len += len;
    frame->PAYLOADLEN = len;
    if (!(bytes->data = malloc(sizeof(char)*bytes->len)))
        return 0;
    // populate the bytes
    bytes->data[0] = (char)((FIN << 7) | (RSV << 4) | (OPCODE));
    bytes->data[1] = (char)(MASK << 7);
    if (len < 126) {
        bytes->data[1] |= len;
    } else if (len > 0xffff) {
        bytes->data[1] |= 127;
        payloadlen_bytes = 8;
    } else {
        bytes->data[1] |= 126;
        payloadlen_bytes = 2;
    }
    // populate payloadlen bytes
    for (int i=0; i < payloadlen_bytes; i++) {
        int shift = 8*(payloadlen_bytes-i-1);
        bytes->data[2+i] = (char)(
            (len & (0xff << shift)) >> shift
        );
    }
    if (MASK) {
        frame->MASKKEY = 2+payloadlen_bytes;
        frame->DATA = 2+payloadlen_bytes+4;
        // populate maskkey bytes
        for (int i=0; i<4; i++)
            // yes we create only a dummy mask
            // the client isn't RFC 6455 compliant
            // TODO: for clients we need to generate a better maskkey
            bytes->data[2+payloadlen_bytes+i] = 0xF0;
    } else {
        frame->DATA = 2+payloadlen_bytes;
    }
    return 1;
}

int sFrame_create_pong(sFrame* pong, sFrame* ping) {
    int status;
    char *pong_data, *ping_data, *pong_maskkey;
    //bytestring data;
    //bytestring_create(&data, &ping->bytes.data[ping->DATA], ping->PAYLOADLEN);
    status = sFrame_create(pong, ping->PAYLOADLEN, 1, 0, OPCODE_PONG, !ping->MASK);
    if (!status)
        return 0;
    pong_data = sFrame_get_data_ptr(pong);
    ping_data = sFrame_get_data_ptr(ping);
    pong_maskkey = sFrame_get_maskkey_ptr(pong);

    if (ping->MASK) {
        // pong doesn't need to be masked
        memcpy(pong_data, ping_data, ping->PAYLOADLEN);
    } else {
        for (size_t i=0; i < ping->PAYLOADLEN; i++) {
            int j = i % 4;
            pong_data[i] = ping_data[i]^pong_maskkey[j];
        }
    }
    //bytestring_delete(&data);
    return 1;
}

int sFrame_create_close(sFrame* close, unsigned code, char* reason, size_t reason_len, int client) {
    int status;
    char* data;
    size_t len = 2+reason_len;
    status = sFrame_create(close, len, 1, 0, OPCODE_CLOSE, client);
    if (!status)
        return 0;
    data = sFrame_get_data_ptr(close);
    data[0] = (code >> 8);
    data[1] = code;
    memcpy(data+2, reason, reason_len);
    return 1;
}

void sFrame_delete(sFrame* frame) {
    bytestring_delete(&frame->bytes);
};

char* sFrame_get_data_ptr(sFrame* frame) {
    return &(frame->bytes.data[frame->DATA]);
};

char* sFrame_get_maskkey_ptr(sFrame* frame) {
    return &(frame->bytes.data[frame->MASKKEY]);
};
