#include "bytestring.h"
#include <stdio.h>
#include <string.h>

void bytestring_print(bytestring* str) {
    bytestring_print_substr(str, 0, 0);
}

void bytestring_print_substr(bytestring* str, size_t frombegin, size_t fromend) {
    unsigned char value;
    printf("b'");
    for (size_t i=frombegin; i < (str->len-fromend); i++) {
        value = (unsigned char) str->data[i];
        if (value == ']')
            printf("\\]");
        else if (value == '[')
            printf("\\[");
        else if (value <= 126 && value >= 32)
            printf("%c", str->data[i]);
        else
            printf("[%02x]", value);
    }
    printf("'");
}

int bytestring_create(bytestring* dststr, char* data, size_t len) {
    char* dstdata;
    if ((dstdata = malloc(len)) != 0)
    {
        memcpy(dstdata, data, len);
        dststr->data = dstdata;
        dststr->len = len;
        return 1;
    }
    return 0;
}

int bytestring_create_empty(bytestring* dststr, size_t len) {
    char* dstdata;
    if ((dstdata = malloc(len)) != 0)
    {
        for (size_t i=0; i<len; i++)
            dstdata[i] = 0;
        dststr->data = dstdata;
        dststr->len = len;
        return 1;
    }
    return 0;
}

int bytestring_add_chararray(bytestring* a, char* bdata, size_t blen) {
    //realloc with size = 0 is impelemntation defined, so we catch it before
    if (blen == 0)
        return 1;
    if (blen < 0) // adding a char should not delete chars
        return 0;
    if ((a->data = realloc(a->data, a->len+blen)) != 0)
    {
        memcpy(&a->data[a->len], bdata, blen);
        a->len += blen;
        return 1;
    }
    bytestring_print(a);
    return 0;
}

int bytestring_shrink(bytestring* string, size_t left, size_t right) {
    size_t len = string->len-left-right;

    if (len < 0)
        return 0;
    else if (len == 0) {
        free(string->data);
        string->data = NULL;
        string->len = 0;
        return 1;
    }

    if (left != 0) {
        if (len < left) // move won't overlap! we can use memcpy
            memcpy(string->data, &string->data[left], len);
        else
            memmove(string->data, &string->data[left], len);
    }

    if (!(string->data = realloc(string->data, len))) {
        return 1;
    }
    string->len = len;
    return 1;
}

void bytestring_delete(bytestring* string) {
    if (string->data != NULL)
        free(string->data);
    string->data = NULL;
}
