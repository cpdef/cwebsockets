#include "http.h"
#include "base64.h"
#include "sha1.h"

#include <time.h>
#include <string.h>

bytestring http_response400()
{
    time_t rawtime;
    bytestring result;
    // lenght of part at line end
    char format[350] = "HTTP/1.1 400 Bad Request\r\n" // 26
                    //6    +5  +3 +4 +5 +3 +3 +3 +3 +2
                    "Date: %a, %d %m %Y %H:%M:%S GMT\r\n" // 37
                    "Server: c_raw_websocket\r\n" // 25
                    "Content-Length: 140\r\n" // 21
                    "Connection: Closed\r\n" // 20
                    "Content-Type: text/html; charset=iso-8859-1\r\n" // 45
                    "\r\n" // 2
                    "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">" // 50
                    "<html>" // 6
                    "<head><title>400 Bad Request</title></head>" // 43
                    "<body><h1>Bad Request</h1></body>" // 33
                    "<html>\r\n"; // 8
    // sum: head(26 +37 +25 +21 +20 +43 +2) + body(+50 +6 +43 +33 +8) + some extra space
    // = head(174) + body(140) + X = 314 + X = 350
    bytestring_create_empty(&result, 350); // TODO: handle memory allocation error
    time(&rawtime);
    strftime(result.data, 350, format, gmtime(&rawtime));
    return result;
}

bytestring http_response500()
{
    time_t rawtime;
    bytestring result;
    // lenght of part at line end
    char format[350] = "HTTP/1.1 500 Internal Server Error\r\n" // 36
                    //6    +5  +3 +4 +5 +3 +3 +3 +3 +2
                    "Date: %a, %d %m %Y %H:%M:%S GMT\r\n" // 37
                    "Server: c_raw_websocket\r\n" // 25
                    "Content-Length: 160\r\n" // 21
                    "Connection: Closed\r\n" // 20
                    "Content-Type: text/html; charset=iso-8859-1\r\n" // 45
                    "\r\n" // 2
                    "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">" // 50
                    "<html>" // 6
                    "<head><title>500 Internal Server Error</title></head>" // 53
                    "<body><h1>Internal Server Error</h1></body>" // 43
                    "<html>\r\n"; // 8
    // sum: head(36 +37 +25 +21 +20 +43 +2) + body(+50 +6 +53 +43 +8) + some extra space
    // = head(184) + body(160) + X = 344 + X = 350
    bytestring_create_empty(&result, 350); // TODO: handle memory allocation error
    time(&rawtime);
    strftime(result.data, 350, format, gmtime(&rawtime));
    return result;
}

void to_lower(char* string) {
    int i = 0;
    while (string[i] != '\0') {
        string[i] |= 32;
        i++;
    }
}

int validate_header(bytestring* h, bytestring* key, char* hostfield_value) {
    bytestring sec_key;
    if(!bytestring_create_empty(&sec_key, 0))
        return HEADER_MEM_ERROR;
    // TODO: check if sec_key is deleted before any return
    // (possible security risk)
    bytestring sha1_result;
    bytestring b64_result;
    int colon = 0, field_index = 0, first_line=1;
    char field_name[100]; // if name/value is longer then 100, crop it
    char field_value[100];

    // 1. first line has to be GET Request: GET <request-uri> HTTP/<version>
    //    where <request-uri> has to be / (we don't support multiple resources).
    //    If thats not the case we send a <HTTP 400 Bad Request>
    //    ???: the version has to be >= 1.1,
    //         maybe its enought to check =1.1
    //         (has to be tried with different browsers)
    //
    // 2. the host field has to match: Host: <HOST>:<PORT>
    // 3. the Upgrade field has to match: Upgrade: websocket (websocket not case sensitive)
    // 4. version field has to match: Sec-WebSocket-Version: 13
    // 5. order of header fields is not relevant
    // 6. The field-name must be composed of printable ASCII characters
    //    (i.e., characters that have values between 33. and 126., decimal, except colon).
    //    The field-body may be composed of any ASCII characters, except CR or LF.
    //    (see: https://stackoverflow.com/questions/47687379/what-characters-are-allowed-in-http-header-values )
    // 7. all unknown header lines are ignored
    //
    // check if complete
    if (!(    h->data[h->len-1] == '\n' && h->data[h->len-2] == '\r'
           && h->data[h->len-3] == '\n' && h->data[h->len-4] == '\r'))
        return HEADER_INCOMPLETE;

    int valid_host = 0, valid_upgrade = 0, valid_connection = 0, valid_key = 0, valid_ws_version = 0;
    for (int i = 0; i < h->len; i++)
    {
        if (h->data[i] == '\r')
        {
            // i+1 can't overflow (we checked that already with the complete check above!)
            if (!(h->data[i+1] == '\n'))
                return HEADER_INVALID;
            field_value[field_index] = '\0';
            if (first_line) {
                first_line = 0;
                // TODO: Allow HTTP/1.1. OR HIGHER GET request
                if (strcmp(field_value, "GET / HTTP/1.1") != 0)
                    return HEADER_INVALID;
            } else {
                if (strcmp(field_name, "Sec-WebSocket-Key") == 0)
                {
                    if (strlen(field_value) != 24)
                        return HEADER_INVALID;
                    if (!bytestring_add_chararray(&sec_key, field_value, 24))
                        return HEADER_MEM_ERROR;
                    valid_key = 1;
                } else if (strcmp(field_name, "Host") == 0) {
                    if (strcmp(field_value, hostfield_value))
                        return HEADER_INVALID;
                    valid_host = 1;
                } else if (strcmp(field_name, "Upgrade") == 0) {
                    to_lower(field_value);
                    if (strcmp(field_value, "websocket"))
                        return HEADER_INVALID;
                    valid_upgrade = 1;
                } else if (strcmp(field_name, "Connection") == 0) {
                    to_lower(field_value);
                    if (strcmp(field_value, "upgrade"))
                        return HEADER_INVALID;
                    valid_connection = 1;
                } else if (strcmp(field_name, "Sec-WebSocket-Version") == 0) {
                    to_lower(field_value);
                    if (strcmp(field_value, "13"))
                        return HEADER_INVALID;
                    valid_ws_version = 1;
                }
            }
            field_name[0] = '\0';
            field_value[0] = '\0';
            colon = 0;
            field_index = 0;
            i++; // skip the line feeds
        } else if (h->data[i] == '\n') {
            return HEADER_INVALID;
        } else {
            if (!colon && !first_line) {
                if (h->data[i] == ':') {
                    field_name[field_index] = '\0';
                    colon = 1;
                    field_index = 0;
                    i++;
                    continue;
                } else if ((unsigned char)h->data[i] >= 33 && (unsigned char)h->data[i] <= 126) {
                    if (field_index > 100)
                        continue;
                    field_name[field_index] = h->data[i];
                } else {
                    return HEADER_INVALID;
                }
            } else {
                // '\r' and '\n' were excluded before, so we don't have to handle them anymore
                if (field_index > 100)
                    continue;
                field_value[field_index] = h->data[i];
            }
            field_index++;
        }
    }

    if (!valid_host || !valid_upgrade || !valid_connection || !valid_key || !valid_ws_version)
        return HEADER_INVALID;
    bytestring_add_chararray(&sec_key, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", 36);
    if(!bytestring_create_empty(&sha1_result, 20))
        return HEADER_MEM_ERROR;
    SHA1(sha1_result.data, sec_key.data, sec_key.len);
    if (!b64encode(&sha1_result, &b64_result))
        return HEADER_MEM_ERROR;
    if (!bytestring_create(key, b64_result.data, b64_result.len))
        return HEADER_MEM_ERROR;
    bytestring_delete(&sec_key);
    bytestring_delete(&sha1_result);
    bytestring_delete(&b64_result);
    return HEADER_VALID;
}

void http_response_websocket_upgrade(bytestring* string, bytestring* key) {
    bytestring_create(string,
        "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\n"
        "Sec-WebSocket-Accept: "
        "############################" // 28x'#' starting at position 96
        "\r\n\r\n", 97+28+4);
    memcpy(&string->data[97], key->data, 28);
}
