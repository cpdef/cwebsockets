#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <arpa/inet.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "websocket.h"

//typedef sWebsocket userdata;
typedef struct {
    sWebsocket* ws;
    struct bufferevent* bev;
} userdata;


// from buffer.h documentation:
// /**
//   Set maximum read buffer size
//   Default is 4096 and it works fine most of time
//   ...
// */
// so we set the read buffer size to 4096
#define READ_BUF_SIZE 4096
#define MAX_CLIENT_NUM 1000

userdata *userdatas[MAX_CLIENT_NUM];
int ticks = 0;

void free_userdata(userdata* data) {
    size_t id = sWebsocket_get_id(data->ws);
    sWebsocket_delete(data->ws);
    bufferevent_free(data->bev);
    free(data);
    userdatas[id] = NULL;
}

void on_message(sWebsocket *ws, sMessage* msg) {
    printf("on message\n");
    if (msg->is_utf8) {
        printf("got message (text):\n");
    } else
        printf("got message (binary):\n");
    if (!sWebsocket_send_message(ws, msg))
        printf("error adding message to send queue!\n");
}

int ws_on_send_bytes_callback(sWebsocket* ws, char* bytes, size_t len) {
    printf("write cb\n");
    size_t id = sWebsocket_get_id(ws);
    bufferevent* bev = userdatas[id]->bev;
    if (bufferevent_write(bev, bytes, len) == 0) {
        return 1;
    }
    return 0;
}

static void echo_write_cb(struct bufferevent* bev, void* ctx) {
    printf("writeable again!\n");
    userdata *data = (userdata*)ctx;
    sWebsocket* ws = data->ws;
    size_t id = sWebsocket_get_id(ws);
    if (!sWebsocket_is_open(ws)) {
        printf("closed state!\n");
        free_userdata(userdatas[id]);
    }
}

static void
echo_read_cb(struct bufferevent *bev, void *ctx)
{
        /* This callback is invoked when there is data to read on bev. */
        userdata *data = (userdata*)ctx;
        sWebsocket* ws = data->ws;
        struct evbuffer *input = bufferevent_get_input(bev);

        char readbuf[READ_BUF_SIZE];
        size_t bufsize = evbuffer_get_length(input);
        if (bufsize > 0) {
            sMessage messages[5];
            size_t msg_count = 5;
            bufferevent_read(bev, readbuf, bufsize);
            sWebsocket_add_bytes(ws, readbuf, bufsize, messages, &msg_count);
            for (unsigned i = 0; i < msg_count; i++)
                on_message(ws, &messages[i]);
        }
}

static void
echo_event_cb(struct bufferevent *bev, short events, void *ctx)
{
        userdata *data = (userdata*)ctx;
        if (events & BEV_EVENT_ERROR) {
            free_userdata(data);
            perror("Error from bufferevent");
        }
        if (events & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
            free_userdata(data);
        }
}

static void
accept_conn_cb(struct evconnlistener *listener,
    evutil_socket_t fd, struct sockaddr *address, int socklen,
    void *ctx)
{
        /* We got a new connection! Set up a bufferevent for it. */
        struct event_base *base = evconnlistener_get_base(listener);
        struct bufferevent *bev = bufferevent_socket_new(
                base, fd, BEV_OPT_CLOSE_ON_FREE);

        for (unsigned i=0; i<MAX_CLIENT_NUM;i++) {
            if (userdatas[i] == NULL) {
                //char hello[] = "Hello from Server!\n:";
                printf("create userdata!\n");
                userdata* data = (userdata*)malloc(sizeof(userdata));
                userdatas[i] = data;
                data->bev = bev;
                printf("bev: %p\n", bev);
                sWebsocket* ws = sWebsocket_new();
                if (!sWebsocket_create(ws, 0, i, "127.0.0.1:9995", DISABLE_SEND_BUFFER)) {
                    break;
                }
                data->ws = ws;

                bufferevent_setcb(bev, echo_read_cb, echo_write_cb, echo_event_cb, (void*)data);
                bufferevent_enable(bev, EV_READ|EV_WRITE);
                //bufferevent_write(bev, hello, strlen(hello));
                return;
            }
        }
        bufferevent_free(bev);
}

static void
accept_error_cb(struct evconnlistener *listener, void *ctx)
{
        struct event_base *base = evconnlistener_get_base(listener);
        int err = EVUTIL_SOCKET_ERROR();
        fprintf(stderr, "Got an error %d (%s) on the listener. "
                "Shutting down.\n", err, evutil_socket_error_to_string(err));

        event_base_loopexit(base, NULL);
}

int
main(int argc, char **argv)
{
        struct event_base *base;
        struct evconnlistener *listener;
        struct sockaddr_in sin;

        int port = 9995;

        if (argc > 1) {
                port = atoi(argv[1]);
        }
        if (port<=0 || port>65535) {
                puts("Invalid port");
                return 1;
        }

        base = event_base_new();
        if (!base) {
                puts("Couldn't open event base");
                return 1;
        }

        /* Clear the sockaddr before using it, in case there are extra
         * platform-specific fields that can mess us up. */
        memset(&sin, 0, sizeof(sin));
        /* This is an INET address */
        sin.sin_family = AF_INET;
        /* Listen on 0.0.0.0 */
        sin.sin_addr.s_addr = htonl(0);
        /* Listen on the given port. */
        sin.sin_port = htons(port);

        for (unsigned i = 0; i<MAX_CLIENT_NUM;i++) {
            userdatas[i] = NULL;
        }
        listener = evconnlistener_new_bind(base, accept_conn_cb, NULL,
            LEV_OPT_CLOSE_ON_FREE|LEV_OPT_REUSEABLE, -1,
            (struct sockaddr*)&sin, sizeof(sin));
        if (!listener) {
                perror("Couldn't create listener");
                return 1;
        }
        evconnlistener_set_error_cb(listener, accept_error_cb);

        set_on_send_bytes_callback(&ws_on_send_bytes_callback);
        while (1) {
            auto now = std::chrono::steady_clock::now();
            event_base_loop(base, EVLOOP_NONBLOCK);
            ticks++;
            std::this_thread::sleep_until(now + std::chrono::milliseconds(10));
        }
        return 0;
}
