/*
** example_selectserver.c -- a websocket multiperson echo server
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include "websocket.h"

#define PORT "8888"   // port we're listening on
#define USE_MESSAGE_CALLBACK

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(void)
{
    fd_set master;    // master file descriptor list
    fd_set read_fds;  // temp file descriptor list for select()
    fd_set write_fds;  // temp file descriptor list for select()
    int fdmax;        // maximum file descriptor number

    int listener;     // listening socket descriptor
    int newfd;        // newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen;

    // websocket declarations/initializations
    sWebsocket** data;

    int nbytes;

    char remoteIP[INET6_ADDRSTRLEN];

    int yes=1;        // for setsockopt() SO_REUSEADDR, below
    int i, rv;

    struct addrinfo hints, *ai, *p;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;

    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        exit(1);
    }

    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) {
            continue;
        }

        // lose the pesky "address already in use" error message
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
            close(listener);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == NULL) {
        fprintf(stderr, "selectserver: failed to bind\n");
        exit(2);
    }

    freeaddrinfo(ai); // all done with this

    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        exit(3);
    }

    // add the listener to the master set
    FD_SET(listener, &master);

    // keep track of the biggest file descriptor
    fdmax = listener; // so far, it's this one

    data = malloc(sizeof(sWebsocket*)*(listener+1));
    data[listener] = NULL;

    // main loop
    for(;;) {
        read_fds = master; // copy it
        write_fds = master; // copy it
        if (select(fdmax+1, &read_fds, &write_fds, NULL, &timeout) == -1) {
            perror("select");
            exit(4);
        }

        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) { // we got one!!
                if (i == listener) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener,
                        (struct sockaddr *)&remoteaddr,
                        &addrlen);
                    fcntl(newfd, F_SETFL, O_NONBLOCK);
                    if (newfd == -1) {
                        perror("accept");
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        if (newfd > fdmax) {    // keep track of the max
                            fdmax = newfd;
                            data = reallocarray(data, (size_t)newfd+1, sizeof(sWebsocket*));
                        }
                        data[newfd] = sWebsocket_new();
                        sWebsocket_create(data[newfd], newfd,0,"127.0.0.1:8888", 0);
                        printf("\x1b[33m[%d] new connection from %s on socket %d\x1b[0m\n", i,
                            inet_ntop(remoteaddr.ss_family,
                                get_in_addr((struct sockaddr*)&remoteaddr),
                                remoteIP, INET6_ADDRSTRLEN),
                            newfd);
                    }
                } else {
                    sMessage message_buffer[5];
                    size_t msg_count = 5;
                    sWebsocket* ws = data[i];
                    nbytes = sWebsocket_recv(ws, message_buffer, &msg_count);
                    for (unsigned i = 0; i < msg_count; i++) {
                        sMessage* message = &message_buffer[i];
                        if (message->is_utf8) {
                            printf("got message (text):\n");
                        }
                        else
                            printf("got message (binary):\n");
                        if (!sWebsocket_send_message(ws, message))
                            printf("error adding message to send queue!\n");
                    }
                } // END handle data from client 
            } // END read
            if (FD_ISSET(i, &write_fds)) { // we got one!!
                sWebsocket* ws = data[i];
                sWebsocket_sendBuffer_send(ws,0);
                // if status is closed and the send buffer is empty, we can delete
                // the websocket, which will also close the internal socket
                if(!sWebsocket_is_open(ws) && sWebsocket_sendBuffer_empty(ws)) {
                    printf("delete websocket!\n");
                    sWebsocket_delete(data[i]);
                    FD_CLR(i, &master); // remove from master set
                }
            }
        } // END looping through file descriptors
        sleep(1/30);
    } // END for(;;)--and you thought it would never end!
    return 0;
}

