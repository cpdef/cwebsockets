#ifndef MESSAGES_H
#define MESSAGES_H

#include "bytestring.h"
#include "frame.h"

typedef struct {
    int is_utf8;
    bytestring data;
} sMessage;

typedef struct __sMessageElement {
    struct __sMessageElement *prev;
    sMessage* data;
} sMessageElement;

void sMessage_delete(sMessage* message);
int sMessage_create(sMessage* message, char* data, size_t len, int is_utf8);
void sMessage_to_frame(sMessage* message, sFrame* frame);


#endif
