#ifndef FRAME_H
#define FRAME_H
#include <stdint.h>
#include "bytestring.h"

enum opcode {
    OPCODE_CONTINUATION = 0,
    OPCODE_TEXT = 0x1,
    OPCODE_BINARY = 0x2,
    OPCODE_NON_CONTOL_0 = 0x3,
    OPCODE_NON_CONTOL_1 = 0x4,
    OPCODE_NON_CONTOL_2 = 0x5,
    OPCODE_NON_CONTOL_3 = 0x6,
    OPCODE_NON_CONTOL_4 = 0x7,
    OPCODE_CLOSE = 0x8,
    OPCODE_PING = 0x9,
    OPCODE_PONG = 0xA,
    OPCODE_CONTROL_0 = 0xB,
    OPCODE_CONTROL_1 = 0xC,
    OPCODE_CONTROL_2 = 0xD,
    OPCODE_CONTROL_3 = 0xE,
    OPCODE_CONTROL_4 = 0xF
};

typedef struct{
    int FIN;
    int RSV;
    int OPCODE;
    int MASK;
    uint64_t PAYLOADLEN;
    size_t MASKKEY;
    size_t DATA;
    bytestring bytes;
} sFrame;

void sFrame_print(sFrame* frame);
int sFrame_create_pong(sFrame* pong, sFrame* ping);
int sFrame_create_close(sFrame* close, unsigned code, char* reason, size_t reason_len, int client);
int sFrame_create(sFrame* frame, size_t len, int FIN, int RSV, int OPCODE, int MASK);
char* sFrame_get_data_ptr(sFrame* frame);
char* sFrame_get_maskkey_ptr(sFrame* frame);
void sFrame_delete(sFrame* frame);
#endif
