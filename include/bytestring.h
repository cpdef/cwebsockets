#ifndef BYTESTRING_H
#define BYTESTRING_H
#include <stdlib.h>
typedef struct {
    char* data;
    size_t len;
} bytestring;

void bytestring_print(bytestring* str);
void bytestring_print_substr(bytestring* str, size_t frombegin, size_t fromend);

int bytestring_create(bytestring* dststr, char* data, size_t len);
int bytestring_create_empty(bytestring* dststr, size_t len);

int bytestring_add_chararray(bytestring* a, char* bdata, size_t blen);
int bytestring_shrink(bytestring* string, size_t left, size_t right);
void bytestring_delete(bytestring* string);
#endif
