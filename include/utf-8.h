#ifndef UTF_8_GUARD
#define UTF_8_GUARD
#include <stddef.h>
int check_utf8(char* string, size_t len);
#endif
