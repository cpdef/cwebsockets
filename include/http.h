#ifndef HTTP_H
#define HTTP_H
#include "bytestring.h"

bytestring http_response400();
bytestring http_response500();
int validate_header(bytestring* h, bytestring* key, char* hostfield_value);
void http_response_websocket_upgrade(bytestring* string, bytestring* key);
enum header_status {
    HEADER_VALID = 0,
    HEADER_INVALID,
    HEADER_INCOMPLETE,
    HEADER_MEM_ERROR
};

#endif
