#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>

#define LOG_VERSION "0.1.0"
#define LOG_USE_COLOR

typedef struct {
  va_list ap;
  const char *fmt;
  const char *file;
  struct tm *time;
  void *udata;
  int line;
  int level;
} wsLog_Event;

typedef void (*wsLog_LogFn)(wsLog_Event *ev);
typedef void (*wsLog_LockFn)(bool lock, void *udata);

enum { LOG_INFO, LOG_SYSTEM, LOG_IN, LOG_OUT, LOG_ERROR };

#define wsLog_info(...)  wsLog_log(LOG_INFO,  __FILE__, __LINE__, __VA_ARGS__)
#define wsLog_system(...) wsLog_log(LOG_SYSTEM, __FILE__, __LINE__, __VA_ARGS__)
#define wsLog_in(...) wsLog_log(LOG_IN, __FILE__, __LINE__, __VA_ARGS__)
#define wsLog_out(...)  wsLog_log(LOG_OUT,  __FILE__, __LINE__, __VA_ARGS__)
#define wsLog_error(...)  wsLog_log(LOG_ERROR,  __FILE__, __LINE__, __VA_ARGS__)
#define nl() puts("");

const char* wsLog_level_string(int level);
void wsLog_set_lock(wsLog_LockFn fn, void *udata);
void wsLog_set_level(int level);
void wsLog_set_quiet(bool enable);
int wsLog_add_callback(wsLog_LogFn fn, void *udata, int level);
int wsLog_add_fp(FILE *fp, int level);

void wsLog_log(int level, const char *file, int line, const char *fmt, ...);

#endif
