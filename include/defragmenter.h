#ifndef DEFRAGMENTER_H
#define DEFRAGMENTER_H
#include "frame.h"
#include "messages.h"

typedef struct sDefragmenter sDefragmenter;

sDefragmenter* sDefragmenter_new();

int sDefragmenter_create(sDefragmenter* df);

int sDefragmenter_run(sDefragmenter* df, sFrame* frame, sMessage* msg);

#endif
