#ifndef FRAME_PARSER_H
#define FRAME_PARSERH

#include "frame.h"
#include "messages.h"


enum frame_parser_result {
    FPARSER_RESULT_PING,
    FPARSER_RESULT_CLOSE,
    FPARSER_RESULT_MESSAGE,
    FPARSER_RESULT_COMPLETE
};

typedef struct sFrameParser sFrameParser;

sFrameParser* sFrameParser_new();
int sFrameParser_create(sFrameParser* parser, int is_server);
int sFrameParser_reset(sFrameParser* parser);
void sFrameParser_addBytes(sFrameParser* parser, char* bytes, int nbytes);
int sFrameParser_parse(sFrameParser* parser, sMessage* message, sFrame** control_frame);
void sFrameParser_delete(sFrameParser* parser);

#endif
