#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdlib.h>

#include "bytestring.h"
#include "messages.h"
#include "frame.h"
#include "send_buffer.h"
#include "log.h"

#ifndef NO_LIBEVENT
#include <event2/bufferevent.h>
#endif

enum websocket_status {
    WEBSOCKET_OPEN,
    WEBSOCKET_CLOSED
};

enum websocket_flags {
    DISABLE_SEND_BUFFER = 1
};

typedef struct sWebsocket sWebsocket;


enum close_codes {
    CLOSE_NORMAL = 0,
    CLOSE_SOCK_RECV_ERROR = 1,
    CLOSE_HTTP_BADREQUEST = 2,
    CLOSE_HTTP_INTSERVERR = 3,
    CLOSE_OK = 1000,
    CLOSE_SHUTDOWN,
    CLOSE_PROTOCOL_ERROR,
    CLOSE_UNKNOWN_DATA_RECEIVED,
    CLOSE_NO_STATUS = 1005,
    CLOSE_APPLICATION_ABNORMALLY,
    CLOSE_NO_UTF8_TEXT_INCONSISTENCY,
    CLOSE_POLICY_VIOLATION,
    CLOSE_TOO_BIG,
    CLOSE_MISSING_EXTENSION,
    CLOSE_UNEXPECTED_CONDITION,
    CLOSE_TLS_HANDSHAKE_FAILURE = 1015
};


sWebsocket* sWebsocket_new();

// the id is used to identify the websocket,
// it is also used in for debug output
// if it is 0, it will be replaced by the socket file descriptor
// if no socket is used, socket has to be set to 0
int sWebsocket_create(sWebsocket* ws, int socket, int id, const char* host, int flags);

/**
 * @return returns the id, specified in sWebsocket_create
 */
int sWebsocket_get_id(sWebsocket* ws);

/**
 * @param max_msg_count [in/out] the value it points to specifies the max
 * number of messages that may be received. After the funtion call it holds
 * the actual number of messages received.
 *
 * @return the number of bytes received (n > 0),
 * or a negative close_code on shutdown/error
 * e.g. if the the peer closes the connection
 * the return value is -CLOSE_NORMAL which is (-)0
 */
int sWebsocket_recv(sWebsocket* ws, sMessage* messages, size_t* max_msg_count);

// This function sets the websocket into "closing" state.
// That means, that the websocket will be closed, as soon as
// it finished sending its last bytes
void sWebsocket_shutdown(sWebsocket* ws, enum close_codes close_code);

/**
 * @return 1 on success and a negative close code else
 * @param max_msg_count [in/out] the value it points to specifies the max
 * number of messages that may be received. After the funtion call it holds
 * the actual number of messages received.
 */
int sWebsocket_add_bytes(sWebsocket *ws, char* bytes, size_t len, sMessage* messages, size_t* max_msg_count);
int sWebsocket_send_message(sWebsocket* ws, sMessage* msg);
int sWebsocket_send_frame(sWebsocket* ws, sFrame* frame);
int sWebsocket_sendBuffer_send(sWebsocket* ws, size_t max_send_len);

int sWebsocket_sendBuffer_empty(sWebsocket* ws); // TODO remove
int sWebsocket_is_open(sWebsocket* ws);

typedef int (*ws_on_send_bytes_callback_t)(sWebsocket* ws, char* bytes, size_t len);
int ws_default_on_send_bytes_callback(sWebsocket* ws, char* bytes, size_t len);
void set_on_send_bytes_callback(ws_on_send_bytes_callback_t callback);

// closes the internal socket and deletes all data
void sWebsocket_delete(sWebsocket* ws);

#ifdef __cplusplus
}
#endif
#endif
