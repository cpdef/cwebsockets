#ifndef SEND_BUFFER_H
#define SEND_BUFFER_H

#include <stdlib.h>

typedef struct {
    size_t internal_size;
    size_t len;
    size_t start;
    char* buf;
} sSendBuffer;

int sSendBuffer_create(sSendBuffer* buffer);
void sSendBuffer_delete(sSendBuffer* buffer);
int sSendBuffer_add_bytes(sSendBuffer* buffer, char* bytes, int len);
int sSendBuffer_send(sSendBuffer* buffer, int socket, size_t max_send_len);
void sSendbuffer_erase(sSendBuffer* buffer);

#endif
