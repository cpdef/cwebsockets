# cwebsockets

This Project is an Websocket Server implementation written in C. A client and a C++ API may follow.

The implementations uses [RFC6455](https://datatracker.ietf.org/doc/html/rfc6455) as an guideline. If you find any errors or mistakes, which make it incompatible with RFC6455 please open an issue.

Until now you might get much (but well formated) debug output, that will change in future,
by implementing an verbosity Flag at Compile time. Though the debug messages might help you understanding the websocket protocol.


## Installation
```
git clone 
cd cwebsockets/build
cmake ..
make && ./example_selectserver
# test with python client:
python3 -m websocket 127.0.0.1:8888
```

## Usage
TBD

## Support
All questions can be asked via the Issue Tracker or via email to cpdef@protonmail.com

## Roadmap
- Test websockets with SSL/TLS
- Add Unit Tests
- Implement higher Level API
- Implement C++ API
- ...

## License
BSD-License

